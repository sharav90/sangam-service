/**
 * 
 */
package com.magalir.sangam.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.magalir.sangam.entity.Sangam;

/**
 * @author Saravanan M
 * @created date 21-Apr-2020 9:26:32 pm
 */
public interface SangamRepo extends JpaRepository<Sangam, Long> {

}
