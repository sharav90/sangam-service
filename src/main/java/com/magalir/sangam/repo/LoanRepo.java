/**
 * 
 */
package com.magalir.sangam.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.magalir.sangam.entity.Loan;

/**
 * @author Saravanan M
 * @created date 23-Apr-2020 8:39:05 pm
 */
public interface LoanRepo extends JpaRepository<Loan, Long> {

}
