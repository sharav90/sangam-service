/**
 * 
 */
package com.magalir.sangam.repo;

import java.math.BigDecimal;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.magalir.sangam.entity.LoanPayment;

/**
 * @author Saravanan M
 * @created date 23-Apr-2020 8:39:28 pm
 */
public interface LoanPaymentRepo extends JpaRepository<LoanPayment, Long> {

	@Query("SELECT SUM(lp.payLoanAmount) FROM LoanPayment lp WHERE lp.loan.id =:loanId")
	BigDecimal findReminingLoanAmountByLoanId(final @Param("loanId") Long loanId);
}
