/**
 * 
 */
package com.magalir.sangam.repo;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.magalir.sangam.entity.Organizer;

/**
 * @author Saravanan M
 * @created date 21-Apr-2020 8:53:53 pm
 */
public interface OrganizerRepo extends JpaRepository<Organizer, Long> {

	Optional<Organizer> findByName(final String username);
}
