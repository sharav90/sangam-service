/**
 * 
 */
package com.magalir.sangam.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.magalir.sangam.entity.SangamMember;

/**
 * @author Saravanan M
 * @created date 22-Apr-2020 11:23:14 am
 */
public interface SangamMemberRepo extends JpaRepository<SangamMember, Long> {

}
