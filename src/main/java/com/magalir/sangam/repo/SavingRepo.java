/**
 * 
 */
package com.magalir.sangam.repo;

import org.springframework.data.jpa.repository.JpaRepository;

import com.magalir.sangam.entity.Saving;

/**
 * @author Saravanan M
 * @created date 23-Apr-2020 8:40:12 pm
 */
public interface SavingRepo extends JpaRepository<Saving, Long> {

}
