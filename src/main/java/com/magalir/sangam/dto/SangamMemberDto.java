/**
 * 
 */
package com.magalir.sangam.dto;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import lombok.Data;

/**
 * @author Saravanan M
 * @created date 25-Apr-2020 5:32:08 pm
 */
@Data
public class SangamMemberDto {
	private Long id;
	private String name;
	private String mobile;
	private String aadharNo;
	private String address;
	private LocalDate dateOfBirth;
	private Boolean isActive;
	private LocalDateTime createdTs;
	private LocalDateTime lastUpdateTs;

	private List<LoanDto> loanDto;
	private List<SavingDto> savingDto;

	private BigDecimal totalSavings;
	private BigDecimal loanAmount;
	private BigDecimal loanRemaining;
	private BigDecimal loanInterestAmount;
}
