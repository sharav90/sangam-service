/**
 * 
 */
package com.magalir.sangam.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.Data;

/**
 * @author Saravanan M
 * @created date 25-Apr-2020 7:12:35 pm
 */
@ApiModel(description = "Loan details for sangam members") // swagger
@Data
public class LoanDto {
	private Long loanId;
	private String loanName;
	@ApiModelProperty(notes = "Loan amount") // swagger
	private BigDecimal loanAmount;
	private Boolean isActive;
	private String status;
	private LocalDateTime createdTs;
	private LocalDateTime lastUpdateTs;
	
	private List<LoanPaymentDto> loanPaymentDto;
}
