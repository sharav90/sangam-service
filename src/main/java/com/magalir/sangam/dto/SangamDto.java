/**
 * 
 */
package com.magalir.sangam.dto;

import java.time.LocalDateTime;

import lombok.Data;

/**
 * @author Saravanan M
 * @created date 26-Apr-2020 3:32:28 pm
 */
@Data
public class SangamDto {
	private Long id;
	private String name;
	private String bankAccNo;
	private String bankName;
	private String ifscCode;
	private Boolean isActive;
	private LocalDateTime createdTs;
	private LocalDateTime lastUpdateTs;
}
