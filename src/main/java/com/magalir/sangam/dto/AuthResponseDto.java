/**
 * 
 */
package com.magalir.sangam.dto;

import lombok.Getter;

/**
 * @author Saravanan M
 * @created date 04-May-2020 8:02:05 pm
 */
@Getter
public class AuthResponseDto {
	
	private final String token;

	public AuthResponseDto(final String token) {
		this.token = token;
	}
}
