/**
 * 
 */
package com.magalir.sangam.dto;

import lombok.Data;

/**
 * @author Saravanan M
 * @created date 04-May-2020 8:00:44 pm
 */
@Data
public class AuthRequestDto {
	
	private String username;
	private String password;

	public AuthRequestDto() {
		super();
	}

	public AuthRequestDto(final String username, final String password) {
		this.setUsername(username);
		this.setPassword(password);
	}
}
