/**
 * 
 */
package com.magalir.sangam.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Data;

/**
 * @author Saravanan M
 * @created date 25-Apr-2020 5:13:02 pm
 */
@Data
public class SavingDto {
	private BigDecimal amount;
	private LocalDateTime createdTs;
}
