/**
 * 
 */
package com.magalir.sangam.dto;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import lombok.Data;

/**
 * @author Saravanan M
 * @created date 25-Apr-2020 5:33:18 pm
 */
@Data
public class OrganizerDto {
	private Long id;
	private String name;
	private String mobile;
	private String passcode;
	private String aadharNo;
	private LocalDate dateOfBirth;
	private String address;
	private Boolean isActive;
	private LocalDateTime createdTs;
	private LocalDateTime lastUpdateTs;
	private List<SangamDto> sangamDto;
}
