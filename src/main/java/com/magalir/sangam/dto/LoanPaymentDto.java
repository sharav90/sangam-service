/**
 * 
 */
package com.magalir.sangam.dto;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import lombok.Data;

/**
 * @author Saravanan M
 * @created date 25-Apr-2020 7:56:38 pm
 */
@Data
public class LoanPaymentDto {
	private BigDecimal loanPayAmount;
	private BigDecimal loanInterestAmount;
	private LocalDateTime createdTs;
}
