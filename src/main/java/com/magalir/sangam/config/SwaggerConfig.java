/**
 * 
 */
package com.magalir.sangam.config;

import java.util.ArrayList;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Saravanan M
 * @created date 05-May-2020 10:01:16 am
 */
@Configuration
@EnableSwagger2
public class SwaggerConfig {

	// http://localhost:8100/v2/api-docs
	// http://localhost:8100/swagger-ui.html

	public static final Contact DEFAULT_CONTACT = new Contact("Saravanan M", "", "sharav90@gmail.com");

	public static final ApiInfo API_INFO = new ApiInfo("Sangam Service Api Documentation", "Sangam Service", "1.0",
			"urn:tos", DEFAULT_CONTACT, "Apache 2.0", "http://www.apache.org/licenses/LICENSE-2.0", new ArrayList<>());

	@Bean
	public Docket api() {
		return new Docket(DocumentationType.SWAGGER_2).apiInfo(API_INFO);
	}
}
