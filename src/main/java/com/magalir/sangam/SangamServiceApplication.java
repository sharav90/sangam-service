package com.magalir.sangam;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//@EnableDiscoveryClient
//@EnableFeignClients
@SpringBootApplication
public class SangamServiceApplication {

	public static void main(final String... args) {
		SpringApplication.run(SangamServiceApplication.class, args);
	}

}
