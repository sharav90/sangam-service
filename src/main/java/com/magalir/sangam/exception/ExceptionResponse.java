/**
 * 
 */
package com.magalir.sangam.exception;

import java.util.Date;

import lombok.Data;

/**
 * @author Saravanan M
 * @created date 28-Apr-2020 4:49:18 pm
 */
@Data
public class ExceptionResponse {
	private int errCode;
	private String errMsg;
	private Date timestamp;
}
