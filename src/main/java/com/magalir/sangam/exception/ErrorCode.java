/**
 * 
 */
package com.magalir.sangam.exception;

/**
 * @author Saravanan M
 * @created date 28-Apr-2020 4:56:54 pm
 */
public enum ErrorCode implements ErrorHandle {
	SANGAM_ERROR_01(01, "Oops!!! Something went wrong, please contact the administrator"),
	SANGAM_ERROR_02(02, "Organizer Id not present in request"), 
	SAMGAM_ERROR_03(03, "Sangam Id not present in request");

	private final int code;
	private final String errMsg;

	ErrorCode(final int errCode, final String errMsg) {
		this.code = errCode;
		this.errMsg = errMsg;
	}

	@Override
	public int getErrCode() {
		return this.code;
	}

	@Override
	public String getErrMsg() {
		return this.errMsg;
	}
}

interface ErrorHandle {
	int getErrCode();

	String getErrMsg();
}