/**
 * 
 */
package com.magalir.sangam.exception;

import java.util.Optional;

import com.magalir.sangam.util.Constants;

/**
 * @author Saravanan M
 * @created date 28-Apr-2020 4:52:31 pm
 */
public class SangamException extends RuntimeException {

	private static final long serialVersionUID = 1L;
	private final ErrorCode error;

	public SangamException(final ErrorCode error) {
		super(getErrMsg(error));
		this.error = error;
	}

	public ErrorCode getError() {
		return error;
	}

	private static String getErrMsg(final ErrorCode error) {
		final Optional<ErrorCode> errorCode = Optional.ofNullable(error);
		return errorCode.isPresent() ? errorCode.get().getErrMsg() : Constants.EMPTY;
	}
}
