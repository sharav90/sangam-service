/**
 * 
 */
package com.magalir.sangam.exception;

import java.util.Date;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import com.magalir.sangam.util.Constants;

/**
 * @author Saravanan M
 * @created date 28-Apr-2020 4:43:15 pm
 */
@ControllerAdvice
public class SangamExceptionHandler extends ResponseEntityExceptionHandler {
	
	private final Logger log = LoggerFactory.getLogger(this.getClass());
	
	@ExceptionHandler({ AuthenticationException.class })
	public ResponseEntity<String> handleAuthenticationException(final AuthenticationException e) {
		return ResponseEntity.status(HttpStatus.UNAUTHORIZED).body(e.getMessage());
	}

	@ExceptionHandler(value = SangamException.class)
	public ResponseEntity<ExceptionResponse> handleSangamException(final SangamException exception) {
		log.error(Constants.EXCEPTION, exception);
		final Optional<ErrorCode> errorCode = Optional.ofNullable(exception.getError());
		return new ResponseEntity<>(getResponse(errorCode), HttpStatus.BAD_REQUEST);
	}

	@ExceptionHandler(value = { Exception.class, RuntimeException.class })
	public ResponseEntity<ExceptionResponse> handleRuntimeException(final Exception exception) {
		log.error(Constants.EXCEPTION, exception);
		final Optional<ErrorCode> errorCode = Optional.ofNullable(ErrorCode.SANGAM_ERROR_01);
		return new ResponseEntity<>(getResponse(errorCode), HttpStatus.INTERNAL_SERVER_ERROR);
	}

	private ExceptionResponse getResponse(final Optional<ErrorCode> errorCode) {
		final ExceptionResponse response = new ExceptionResponse();
		if (errorCode.isPresent()) {
			final ErrorCode error = errorCode.get();
			response.setErrCode(error.getErrCode());
			response.setErrMsg(error.getErrMsg());
			response.setTimestamp(new Date());
		}
		log.error(Constants.EXCEPTION_RESPONSE, response);
		return response;
	}
}
