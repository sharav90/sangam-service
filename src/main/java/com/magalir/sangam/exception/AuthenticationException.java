/**
 * 
 */
package com.magalir.sangam.exception;

/**
 * @author Saravanan M
 * @created date 04-May-2020 8:20:50 pm
 */
public class AuthenticationException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public AuthenticationException(final String message, final Throwable cause) {
		super(message, cause);
	}
}
