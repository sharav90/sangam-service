/**
 * 
 */
package com.magalir.sangam.util;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.magalir.sangam.dto.LoanDto;
import com.magalir.sangam.dto.LoanPaymentDto;
import com.magalir.sangam.dto.OrganizerDto;
import com.magalir.sangam.dto.SangamDto;
import com.magalir.sangam.dto.SangamMemberDto;
import com.magalir.sangam.dto.SavingDto;
import com.magalir.sangam.entity.Loan;
import com.magalir.sangam.entity.LoanPayment;
import com.magalir.sangam.entity.Organizer;
import com.magalir.sangam.entity.Sangam;
import com.magalir.sangam.entity.SangamMember;
import com.magalir.sangam.entity.Saving;

/**
 * @author Saravanan M
 * @created date 05-May-2020 4:27:16 pm
 */
public final class MapperUtil {

	private static Logger log = LoggerFactory.getLogger(MapperUtil.class);

	public static Organizer mapDtoToOrganizer(final OrganizerDto dto) {
		final Organizer organizer = new Organizer();
		organizer.setName(dto.getName());
		organizer.setPasscode(dto.getPasscode());
		organizer.setMobile(dto.getMobile());
		organizer.setDateOfBirth(dto.getDateOfBirth());
		organizer.setAadharNo(dto.getAadharNo());
		organizer.setAddress(dto.getAddress());
		return organizer;
	}

	public static List<OrganizerDto> mapOrganizerListToDto(final List<Organizer> organizerList) {
		final List<OrganizerDto> organizerDto;
		if (ValidationUtil.isEmptyCollection(organizerList)) {
			organizerDto = null;
		} else {
			organizerDto = new ArrayList<>();
			organizerList.forEach(organizer -> organizerDto.add(mapOrganizerToDto(organizer)));
		}
		return organizerDto;
	}

	public static OrganizerDto mapOrganizerToDto(final Organizer organizer) {
		final OrganizerDto organizerDto = new OrganizerDto();
		organizerDto.setId(organizer.getId());
		organizerDto.setName(organizer.getName());
		organizerDto.setMobile(organizer.getMobile());
		organizerDto.setAadharNo(organizer.getAadharNo());
		organizerDto.setDateOfBirth(organizer.getDateOfBirth());
		organizerDto.setAddress(organizer.getAddress());
		organizerDto.setIsActive(organizer.getIsActive());
		organizerDto.setCreatedTs(organizer.getCreatedTs());
		organizerDto.setLastUpdateTs(organizer.getLastUpdateTs());
		// SangamDto
		final List<Sangam> sangamList = organizer.getSangamList();
		organizerDto.setSangamDto(MapperUtil.mapSangamListToDto(sangamList));
		return organizerDto;
	}

	public static List<SangamDto> mapSangamListToDto(final List<Sangam> sangamList) {
		final List<SangamDto> sangamDto;
		if (ValidationUtil.isEmptyCollection(sangamList)) {
			sangamDto = null;
		} else {
			sangamDto = new ArrayList<>();
			sangamList.forEach(sangam -> sangamDto.add(mapSangamToDto(sangam)));
		}
		return sangamDto;
	}

	public static SangamDto mapSangamToDto(final Sangam sangam) {
		final SangamDto sangamDto = new SangamDto();
		sangamDto.setId(sangam.getId());
		sangamDto.setName(sangam.getName());
		sangamDto.setBankAccNo(sangam.getBankAccNo());
		sangamDto.setBankName(sangam.getBankName());
		sangamDto.setIfscCode(sangam.getIfscCode());
		sangamDto.setIsActive(sangam.getIsActive());
		sangamDto.setCreatedTs(sangam.getCreatedTs());
		sangamDto.setLastUpdateTs(sangam.getLastUpdateTs());
		return sangamDto;
	}

	public static List<SangamMemberDto> mapSangammeberListToDto(final List<SangamMember> sangamMemberList) {
		final List<SangamMemberDto> memberDtoList;
		if (ValidationUtil.isEmptyCollection(sangamMemberList)) {
			memberDtoList = null;
		} else {
			memberDtoList = new ArrayList<>();
			sangamMemberList.forEach(sangamMember -> memberDtoList.add(mapSangamMemberToDto(sangamMember)));
		}
		return memberDtoList;
	}

	public static SangamMemberDto mapSangamMemberToDto(final SangamMember sangamMember) {
		final SangamMemberDto memberDto = new SangamMemberDto();
		memberDto.setId(sangamMember.getId());
		memberDto.setName(sangamMember.getName());
		memberDto.setMobile(sangamMember.getMobile());
		memberDto.setAadharNo(sangamMember.getAadharNo());
		memberDto.setDateOfBirth(sangamMember.getDateOfBirth());
		memberDto.setAddress(sangamMember.getAddress());
		memberDto.setCreatedTs(sangamMember.getCreatedTs());
		memberDto.setLastUpdateTs(sangamMember.getLastUpdateTs());
		memberDto.setIsActive(sangamMember.getIsActive());
		// savings
		final List<SavingDto> savingListDto = mapSavingListToDto(sangamMember.getSavingList());
		memberDto.setSavingDto(savingListDto);
		final BigDecimal totalSavings = getTotalSavings(savingListDto);
		memberDto.setTotalSavings(totalSavings);
		// loan
		final List<LoanDto> loanListDto = mapLoanListToDto(sangamMember.getLoanList());
		memberDto.setLoanDto(loanListDto);
		setLoanAmount(memberDto, loanListDto);
		return memberDto;
	}

	private static BigDecimal getTotalSavings(final List<SavingDto> savingListDto) {
		final BigDecimal totalSavings;
		if (ValidationUtil.isEmptyCollection(savingListDto)) {
			totalSavings = BigDecimal.ZERO;
		} else {
			totalSavings = savingListDto.stream()
					.map(savings -> Optional.ofNullable(savings.getAmount()).orElse(BigDecimal.ZERO))
					.reduce(BigDecimal.ZERO, BigDecimal::add);
		}
		return totalSavings;
	}

	private static void setLoanAmount(final SangamMemberDto memberDto, final List<LoanDto> loanListDto) {
		BigDecimal loanAmount = BigDecimal.ZERO;
		BigDecimal totalPaidLoanAmount = BigDecimal.ZERO;
		BigDecimal totalPaidInterestAmount = BigDecimal.ZERO;
		valBk: {
			if (ValidationUtil.isEmptyCollection(loanListDto)) {
				log.info("User not taken any loan, SangamMeber Id={}, Name={}", memberDto.getId(), memberDto.getName());
				break valBk;
			}
			final Optional<LoanDto> activeLoan = loanListDto.stream().filter(loan -> loan.getIsActive()).findFirst();
			if (!activeLoan.isPresent()) {
				log.info("No active loan for the user, SangamMeber Id={}, Name={}", memberDto.getId(),
						memberDto.getName());
				break valBk;
			}
			final LoanDto loanDto = activeLoan.get();
			loanAmount = loanDto.getLoanAmount();
			final List<LoanPaymentDto> loanPaymentDto = loanDto.getLoanPaymentDto();
			if (ValidationUtil.isEmptyCollection(loanPaymentDto)) {
				log.info("Loan Payment is empty, SangamMeber Id={}, Name={}", memberDto.getId(), memberDto.getName());
				break valBk;
			}
			totalPaidLoanAmount = loanPaymentDto.stream()
					.map(loanPay -> Optional.ofNullable(loanPay.getLoanPayAmount()).orElse(BigDecimal.ZERO))
					.reduce(BigDecimal.ZERO, BigDecimal::add);
			totalPaidInterestAmount = loanPaymentDto.stream()
					.map(loanPay -> Optional.ofNullable(loanPay.getLoanInterestAmount()).orElse(BigDecimal.ZERO))
					.reduce(BigDecimal.ZERO, BigDecimal::add);
		}
		memberDto.setLoanAmount(loanAmount);
		// remainingLoanAmount = loanAmount - totalLoanPaidAmount
		memberDto.setLoanRemaining(loanAmount.subtract(totalPaidLoanAmount));
		memberDto.setLoanInterestAmount(totalPaidInterestAmount);
	}

	public static List<SavingDto> mapSavingListToDto(final List<Saving> savingList) {
		final List<SavingDto> savingDtoList;
		if (ValidationUtil.isEmptyCollection(savingList)) {
			savingDtoList = null;
		} else {
			savingDtoList = new ArrayList<>();
			savingList.forEach(saving -> savingDtoList.add(mapSavingToDto(saving)));
		}
		return savingDtoList;
	}

	public static SavingDto mapSavingToDto(final Saving saving) {
		final SavingDto savingDto = new SavingDto();
		savingDto.setAmount(saving.getAmount());
		savingDto.setCreatedTs(saving.getCreatedTs());
		return savingDto;
	}

	public static List<LoanDto> mapLoanListToDto(final List<Loan> loanList) {
		final List<LoanDto> loanDtoList;
		if (ValidationUtil.isEmptyCollection(loanList)) {
			loanDtoList = null;
		} else {
			loanDtoList = new ArrayList<>();
			loanList.forEach(loan -> loanDtoList.add(mapLoanToDto(loan)));
		}
		return loanDtoList;
	}

	public static LoanDto mapLoanToDto(final Loan loan) {
		final LoanDto loanDto = new LoanDto();
		final Long loanId = loan.getId();
		loanDto.setLoanId(loanId);
		loanDto.setLoanName(loan.getLoanName());
		loanDto.setLoanAmount(loan.getLoanAmount());
		loanDto.setCreatedTs(loan.getCreatedTs());
		final Boolean isActive = loan.getIsActive();
		loanDto.setStatus(isActive ? Constants.IN_PROGRESS : Constants.DONE);
		loanDto.setIsActive(isActive);
		final List<LoanPaymentDto> loanPaymentDtoList = mapLoanPaymentListToDto(loan.getLoanPaymentList());
		loanDto.setLoanPaymentDto(loanPaymentDtoList);
		return loanDto;
	}

	public static List<LoanPaymentDto> mapLoanPaymentListToDto(final List<LoanPayment> loanPaymentList) {
		final List<LoanPaymentDto> loanPaymentDtoList;
		if (ValidationUtil.isEmptyCollection(loanPaymentList)) {
			loanPaymentDtoList = null;
		} else {
			loanPaymentDtoList = new ArrayList<>();
			loanPaymentList.forEach(loanPay -> loanPaymentDtoList.add(mapLoanPaymentToDto(loanPay)));
		}
		return loanPaymentDtoList;
	}

	public static LoanPaymentDto mapLoanPaymentToDto(final LoanPayment loanPayment) {
		final LoanPaymentDto loanPaymentDto = new LoanPaymentDto();
		loanPaymentDto.setLoanPayAmount(loanPayment.getPayLoanAmount());
		loanPaymentDto.setLoanInterestAmount(loanPayment.getPayInterestAmount());
		loanPaymentDto.setCreatedTs(loanPayment.getCreatedTs());
		return loanPaymentDto;
	}
}
