/**
 * 
 */
package com.magalir.sangam.util;

import java.time.LocalDate;
import java.util.Date;
import java.util.Optional;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.magalir.sangam.util.DateUtil;

/**
 * @author Saravanan M
 * @created date 27-Apr-2020 6:58:31 pm
 */
@Converter(autoApply = true)
public class LocalDateAttributeConverter implements AttributeConverter<LocalDate, Date> {

	@Override
	public Date convertToDatabaseColumn(final LocalDate localDate) {
		final Optional<LocalDate> date = Optional.ofNullable(localDate);
		return date.isPresent() ? DateUtil.convertLocalDateToDate(date.get()) : null;
	}

	@Override
	public LocalDate convertToEntityAttribute(final Date dbDate) {
		final Optional<Date> date = Optional.ofNullable(dbDate);
		return date.isPresent() ? DateUtil.convertDateToLocalDate(date.get()) : null;
	}

}
