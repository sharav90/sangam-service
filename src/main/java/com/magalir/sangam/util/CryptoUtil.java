/**
 * 
 */
package com.magalir.sangam.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Saravanan M
 * @created date 06-May-2020 4:41:26 pm
 */
public final class CryptoUtil {

	public static String getEncodePasscode(final String username, final String passcode) {
		final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
//		final StringBuilder passcodeSb = new StringBuilder("sangam")
//				.append(username)
//				.append("-")
//				.append(passcode)
//				.append("service");
		return encoder.encode(passcode);
	}

}
