/**
 * 
 */
package com.magalir.sangam.util;

/**
 * @author Saravanan M
 * @created date 23-Apr-2020 8:43:02 pm
 */
public interface Constants {

	// Database Name
	String DB_NAME = "SANGAM";

	// Table Names
	String ORGANIZER = "ORGANIZER";
	String SANGAM_MASTER = "SANGAM_MASTER";
	String SANGAM_MEMBERS = "SANGAM_MEMBERS";
	String MEMBERS_SAVINGS = "MEMBER_SAVINGS";
	String LOAN_MASTER = "LOAN_MASTER";
	String LOAN_PAYMENTS = "LOAN_PAYMENTS";
	
	// HttpRequest Header Params
	String ORGANIZER_ID = "organizerId";
	String SANGAM_ID = "sangamId";
	String SANGAM_MEMBER_ID = "sangamMemberId";
	String LOAN_ID = "loanId";

	// Loan status
	String IN_PROGRESS = "In Progress";
	String DONE = "Done";

	String EMPTY = "";

	// Logger
	String EXCEPTION = "Exception: {}";
	String EXCEPTION_RESPONSE = "Exception response: {}";

}
