/**
 * 
 */
package com.magalir.sangam.util;

import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;

/**
 * @author Saravanan M
 * @created date 04-May-2020 8:43:16 pm
 */
public class BCryptEncodeTest {

	public static void main(final String... args) {
		BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		String encodeStr = encoder.encode("Pass@123");
		System.out.println(encodeStr);
	}

	public static String encodePasscode(final String username, final String passcode) {
		final BCryptPasswordEncoder encoder = new BCryptPasswordEncoder();
		final StringBuilder passcodeSb = new StringBuilder(username).append(passcode).append("sangam-service");
		return encoder.encode(passcodeSb.toString());
	}

}
