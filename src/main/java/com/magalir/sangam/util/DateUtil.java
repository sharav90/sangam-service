/**
 * 
 */
package com.magalir.sangam.util;

import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Optional;

/**
 * @author Saravanan M
 * @created date 23-Apr-2020 8:46:00 pm
 */
public interface DateUtil {

	String EMPTY = "";

	String DATETIME_FORMAT = "dd-MM-yyyy HH:mm:ss";

	LocalDateTime DATE_TIME_NOW = LocalDateTime.now();

	DateTimeFormatter DATETIME_FORMATTER = DateTimeFormatter.ofPattern(DATETIME_FORMAT);

	static String convertDateTimeToString(final LocalDateTime dateTime) {
		final Optional<LocalDateTime> dateTimeOptional = Optional.ofNullable(dateTime);
		return dateTimeOptional.isPresent() ? DATETIME_FORMATTER.format(dateTimeOptional.get()) : EMPTY;
	}

	static LocalDateTime convertStringToDateTime(final String dateTimeStr) {
		final Optional<String> dateTimeOptional = Optional.ofNullable(dateTimeStr);
		return dateTimeOptional.isPresent() ? LocalDateTime.parse(dateTimeOptional.get(), DATETIME_FORMATTER) : null;
	}

	static Date convertDateTimeToDate(final LocalDateTime localDateTime) {
		final Optional<LocalDateTime> dateTimeOptional = Optional.ofNullable(localDateTime);
		return dateTimeOptional.isPresent()
				? Date.from(dateTimeOptional.get().atZone(ZoneId.systemDefault()).toInstant())
				: null;
	}

	static LocalDateTime convertDateToDateTime(final Date date) {
		final Optional<Date> dateOptional = Optional.ofNullable(date);
		return dateOptional.isPresent()
				? LocalDateTime.ofInstant(dateOptional.get().toInstant(), ZoneId.systemDefault())
				: null;
	}

	static Date convertLocalDateToDate(final LocalDate localDate) {
		final Optional<LocalDate> date = Optional.ofNullable(localDate);
		return date.isPresent() ? Date.from(date.get().atStartOfDay().atZone(ZoneId.systemDefault()).toInstant())
				: null;
	}

	static LocalDate convertDateToLocalDate(final Date date) {
		final Optional<Date> dateOptional = Optional.ofNullable(date);
		return dateOptional.isPresent()
				? Instant.ofEpochMilli(dateOptional.get().getTime()).atZone(ZoneId.systemDefault()).toLocalDate()
				: null;
	}
}
