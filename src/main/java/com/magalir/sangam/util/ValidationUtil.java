/**
 * 
 */
package com.magalir.sangam.util;

import java.util.Collection;
import java.util.Map;
import java.util.Optional;

/**
 * @author Saravanan M
 * @created date 23-Apr-2020 8:45:20 pm
 */
public interface ValidationUtil {

	/**
	 * Method check the string value is empty or not
	 * 
	 * @param strVal
	 * @return boolean - true if value is empty else false
	 */
	public static boolean isEmptyString(final String strVal) {
		return Optional.ofNullable(strVal).orElse("").trim().isEmpty();
	}

	/**
	 * Method check the collection is empty or not
	 * 
	 * @param collection
	 * @return boolean - true if collection is empty else false
	 */
	public static boolean isEmptyCollection(final Collection<?> collection) {
		return collection == null || collection.isEmpty();
	}

	/**
	 * Method check the map is empty or not
	 * 
	 * @param map
	 * @return boolean - true if map is empty else false
	 */
	public static boolean isEmptyMap(final Map<?, ?> map) {
		return map == null || map.isEmpty();
	}
}
