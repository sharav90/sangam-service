/**
 * 
 */
package com.magalir.sangam.util;

import java.time.LocalDateTime;
import java.util.Date;
import java.util.Optional;

import javax.persistence.AttributeConverter;
import javax.persistence.Converter;

import com.magalir.sangam.util.DateUtil;

/**
 * @author Saravanan M
 * @created date 26-Apr-2020 9:14:28 am
 */
@Converter(autoApply = true)
public class LocalDateTimeAttributeConverter implements AttributeConverter<LocalDateTime, Date> {

	@Override
	public Date convertToDatabaseColumn(final LocalDateTime dateTime) {
		final Optional<LocalDateTime> locDateTime = Optional.ofNullable(dateTime);
		return locDateTime.isPresent() ? DateUtil.convertDateTimeToDate(locDateTime.get()) : null;
	}

	@Override
	public LocalDateTime convertToEntityAttribute(final Date dbDate) {
		final Optional<Date> date = Optional.ofNullable(dbDate);
		return date.isPresent() ? DateUtil.convertDateToDateTime(date.get()) : null;
	}

}
