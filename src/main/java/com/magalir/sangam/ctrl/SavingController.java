/**
 * 
 */
package com.magalir.sangam.ctrl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.magalir.sangam.entity.Saving;
import com.magalir.sangam.svc.SavingService;
import com.magalir.sangam.util.Constants;

/**
 * @author Saravanan M
 * @created date 24-Apr-2020 6:45:01 pm
 */
@RestController
@RequestMapping("/sangam")
public class SavingController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	private @Autowired SavingService service;

	/**
	 * Method will create saving.
	 * 
	 * @param sangamMemId
	 * @param saving
	 * @return Saving
	 */
	@PostMapping("/saving")
	public ResponseEntity<Saving> paySaving(final @RequestParam(Constants.SANGAM_MEMBER_ID) Long sangamMemId,
			final @RequestBody Saving saving) {
		log.info("Pay saving init");
		return new ResponseEntity<>(service.paySaving(sangamMemId, saving), HttpStatus.CREATED);
	}
}
