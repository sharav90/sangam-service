/**
 * 
 */
package com.magalir.sangam.ctrl;

import java.util.Objects;

import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.magalir.sangam.dto.AuthRequestDto;
import com.magalir.sangam.dto.AuthResponseDto;
import com.magalir.sangam.exception.AuthenticationException;
import com.magalir.sangam.jwt.JwtTokenUtil;
import com.magalir.sangam.jwt.JwtUserDetails;
import com.magalir.sangam.svc.AuthService;

/**
 * @author Saravanan M
 * @created date 13-Apr-2020 8:14:36 pm
 */
@CrossOrigin("*")
@RestController
public class AuthController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	private @Autowired JwtTokenUtil jwtTokenUtil;
	private @Autowired AuthenticationManager authManager;
	private @Autowired AuthService authService;
	private @Value("${jwt.http.request.header}") String tokenHeader;

	@PostMapping(value = "${jwt.get.token.uri}")
	public ResponseEntity<?> createAuthenticationToken(final @RequestBody AuthRequestDto authReqst)
			throws AuthenticationException {
		authenticate(authReqst.getUsername(), authReqst.getPassword());
		final UserDetails userDetails = authService.loadUserByUsername(authReqst.getUsername());
		final String token = jwtTokenUtil.generateToken(userDetails);
		return ResponseEntity.ok(new AuthResponseDto(token));
	}

	private void authenticate(final String username, final String password) {
		Objects.requireNonNull(username);
		Objects.requireNonNull(password);
		try {
			authManager.authenticate(new UsernamePasswordAuthenticationToken(username, password));
		} catch (DisabledException e) {
			log.error("Authentication exception: {}", e);
			throw new AuthenticationException("USER_DISABLED", e);
		} catch (BadCredentialsException e) {
			log.error("Authentication exception: {}", e);
			throw new AuthenticationException("INVALID_CREDENTIALS", e);
		}
	}

	@GetMapping(value = "${jwt.refresh.token.uri}")
	public ResponseEntity<?> refreshAndGetAuthenticationToken(final HttpServletRequest request) {
		final String authToken = request.getHeader(tokenHeader);
		final String token = authToken.substring(7);
		final String username = jwtTokenUtil.getUsernameFromToken(token);
		@SuppressWarnings("unused")
		final JwtUserDetails user = (JwtUserDetails) authService.loadUserByUsername(username);
		if (jwtTokenUtil.canTokenBeRefreshed(token)) {
			final String refreshedToken = jwtTokenUtil.refreshToken(token);
			return ResponseEntity.ok(new AuthResponseDto(refreshedToken));
		} else {
			return ResponseEntity.badRequest().body(null);
		}
	}

}
