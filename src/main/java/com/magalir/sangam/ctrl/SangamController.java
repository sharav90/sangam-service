/**
 * 
 */
package com.magalir.sangam.ctrl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.magalir.sangam.dto.LoanDto;
import com.magalir.sangam.dto.SangamMemberDto;
import com.magalir.sangam.entity.Sangam;
import com.magalir.sangam.entity.SangamMember;
import com.magalir.sangam.svc.SangamService;
import com.magalir.sangam.util.Constants;

/**
 * @author Saravanan M
 * @created date 24-Apr-2020 6:42:45 pm
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/sangam")
public class SangamController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	private @Autowired SangamService service;

	/**
	 * Method will create Sangam.
	 * 
	 * @param organizerId
	 * @param sangam
	 * @return Sangam
	 */
	@PostMapping("/sangam")
	public ResponseEntity<Sangam> createSangam(final @RequestParam(Constants.ORGANIZER_ID) Long organizerId,
			final @RequestBody Sangam sangam) {
		log.info("Create sangam init");
		return new ResponseEntity<>(service.createSangam(organizerId, sangam), HttpStatus.CREATED);
	}

	/**
	 * Method will create SangamMember.
	 * 
	 * @param sangamId
	 * @param sangamMember
	 * @return SangamMember
	 */
	@PostMapping("/sangammember")
	public ResponseEntity<SangamMember> createMember(final @RequestParam(Constants.SANGAM_ID) Long sangamId,
			final @RequestBody SangamMember sangamMember) {
		return new ResponseEntity<>(service.createSangamMember(sangamId, sangamMember), HttpStatus.CREATED);
	}

	/**
	 * Find all SangamMember's.
	 * 
	 * @return List<SangamMemberDto>
	 */
	@GetMapping("/sangammember")
	public ResponseEntity<List<SangamMemberDto>> findAllSangamMembers() {
		return new ResponseEntity<>(service.findAllSangamMembers(), HttpStatus.OK);
	}

	/**
	 * Find SangamMember by Id.
	 * 
	 * @param sangamMemId
	 * @return SangamMemInfoDto
	 */
	@GetMapping("/sangammember/{id}")
	public ResponseEntity<SangamMemberDto> findSangamMemberById(final @PathVariable("id") Long sangamMemId) {
		return new ResponseEntity<>(service.findSangamMemberById(sangamMemId), HttpStatus.OK);
	}

	/**
	 * Find Loan by sangamMemberId and loanId.
	 * 
	 * @param sangamMemId
	 * @param loanId
	 * @return LoanDto
	 */
	@GetMapping("/sangammember/{id}/loan/{loanId}")
	public ResponseEntity<LoanDto> findLoanInfoByLoanId(final @PathVariable("id") Long sangamMemId,
			final @PathVariable("loanId") Long loanId) {
		return new ResponseEntity<>(service.findLoanInfoByLoanId(sangamMemId, loanId), HttpStatus.OK);
	}
}
