/**
 * 
 */
package com.magalir.sangam.ctrl;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.magalir.sangam.dto.OrganizerDto;
import com.magalir.sangam.svc.OrganizerService;

/**
 * @author Saravanan M
 * @created date 24-Apr-2020 6:44:17 pm
 */
@CrossOrigin("*")
@RestController
@RequestMapping("/sangam")
public class OrganizerController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	private @Autowired OrganizerService service;

	/**
	 * Method will create organizer.
	 * 
	 * @param organizerDto
	 * @return OrganizerDto
	 */
	@PostMapping("/organizer")
	public ResponseEntity<OrganizerDto> save(final @RequestBody OrganizerDto organizerDto) {
		log.info("Create organizer init");
		return new ResponseEntity<>(service.save(organizerDto), HttpStatus.CREATED);
	}

	/**
	 * Find all organizer's.
	 * 
	 * @return List<OrganizerDto>
	 */
	//@GetMapping("/organizer")
	public ResponseEntity<List<OrganizerDto>> findAll() {
		return new ResponseEntity<>(service.findAll(), HttpStatus.OK);
	}

	/**
	 * Find organizer by id.
	 * 
	 * @param id
	 * @return OrganizerDto
	 */
	@GetMapping("/organizer/{id}")
	public ResponseEntity<OrganizerDto> findById(final @PathVariable("id") Long id) {
		return new ResponseEntity<>(service.findById(id), HttpStatus.OK);
	}

	@GetMapping("/organizer")
	public ResponseEntity<OrganizerDto> findByName(final @RequestParam("name") String name) {
		return new ResponseEntity<>(service.findByName(name), HttpStatus.OK);
	}

	/**
	 * Method will update organizer by Id.
	 * 
	 * @param id
	 * @param organizerDto
	 * @return OrganizerDto
	 */
	@PutMapping("/organizer/{id}")
	public ResponseEntity<OrganizerDto> updateById(final @PathVariable("id") Long id,
			final @RequestBody OrganizerDto organizerDto) {
		return null;
	}
}
