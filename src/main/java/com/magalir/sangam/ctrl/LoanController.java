/**
 * 
 */
package com.magalir.sangam.ctrl;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.magalir.sangam.entity.Loan;
import com.magalir.sangam.entity.LoanPayment;
import com.magalir.sangam.svc.LoanService;
import com.magalir.sangam.util.Constants;

/**
 * @author Saravanan M
 * @created date 24-Apr-2020 6:45:20 pm
 */
@RestController
@RequestMapping("/sangam")
public class LoanController {

	private Logger log = LoggerFactory.getLogger(this.getClass());

	private @Autowired LoanService service;

	/**
	 * Method will create loan.
	 * 
	 * @param sangamMemId
	 * @param loan
	 * @return Loan
	 */
	@PostMapping("/loan")
	public ResponseEntity<Loan> createLoan(final @RequestParam(Constants.SANGAM_MEMBER_ID) Long sangamMemId,
			final @RequestBody Loan loan) {
		log.info("Create loan init");
		return new ResponseEntity<>(service.createLoan(sangamMemId, loan), HttpStatus.CREATED);
	}

	/**
	 * Method will create LoanPayment.
	 * 
	 * @param loanId
	 * @param loanPayment
	 * @return LoanPayment
	 */
	@PostMapping("/loanpay")
	public ResponseEntity<LoanPayment> payLoan(final @RequestParam(Constants.LOAN_ID) Long loanId,
			final @RequestBody LoanPayment loanPayment) {
		return new ResponseEntity<>(service.payLoan(loanId, loanPayment), HttpStatus.CREATED);
	}
}
