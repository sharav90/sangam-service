/**
 * 
 */
package com.magalir.sangam.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.magalir.sangam.util.Constants;

import lombok.Data;

/**
 * @author Saravanan M
 * @created date 23-Apr-2020 7:56:15 pm
 */
@Data
@Entity
@Table(name = Constants.LOAN_PAYMENTS)
public class LoanPayment {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "PAY_LOAN_AMOUNT")
	private BigDecimal payLoanAmount;

	@Column(name = "PAY_INTEREST_AMOUNT")
	private BigDecimal payInterestAmount;

	@Column(name = "CREATED_TS")
	private LocalDateTime createdTs;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "LOAN_ID")
	private Loan loan;
}
