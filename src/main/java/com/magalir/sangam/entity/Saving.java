/**
 * 
 */
package com.magalir.sangam.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.magalir.sangam.util.Constants;

import lombok.Data;

/**
 * @author Saravanan M
 * @created date 23-Apr-2020 7:40:43 pm
 */
@Data
@Entity
@Table(name = Constants.MEMBERS_SAVINGS)
public class Saving {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "SAVINGS_AMOUNT")
	private BigDecimal amount;

	@Column(name = "CREATED_TS")
	private LocalDateTime createdTs;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "SANGAM_MEMBER_ID")
	private SangamMember sangamMember;
}
