/**
 * 
 */
package com.magalir.sangam.entity;

import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.magalir.sangam.util.Constants;

import lombok.Data;

/**
 * @author Saravanan M
 * @created date 21-Apr-2020 3:15:39 pm
 */
@Data
@Entity
@Table(name = Constants.SANGAM_MASTER)
public class Sangam {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "BANK_ACC_NO")
	private String bankAccNo;

	@Column(name = "BANK_NAME")
	private String bankName;

	@Column(name = "IFSC_CODE")
	private String ifscCode;

	@Column(name = "IS_ACTIVE")
	private Boolean isActive;

	@Column(name = "CREATED_TS")
	private LocalDateTime createdTs;

	@Column(name = "LAST_UPDATE_TS")
	private LocalDateTime lastUpdateTs;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.ALL, optional = false)
	@JoinColumn(name = "ORGANIZER_ID")
	private Organizer organizer;

	@JsonIgnore
	@OneToMany(mappedBy = "sangam", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<SangamMember> sangamMemberList;
}
