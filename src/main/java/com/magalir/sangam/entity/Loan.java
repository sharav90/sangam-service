/**
 * 
 */
package com.magalir.sangam.entity;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.magalir.sangam.util.Constants;

import lombok.Data;

/**
 * @author Saravanan M
 * @created date 23-Apr-2020 7:55:49 pm
 */
@Data
@Entity
@Table(name = Constants.LOAN_MASTER)
public class Loan {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "LOAN_NAME")
	private String loanName;

	@Column(name = "LOAN_AMOUNT")
	private BigDecimal loanAmount;

	@Column(name = "IS_ACTIVE")
	private Boolean isActive;

	@Column(name = "CREATED_TS")
	private LocalDateTime createdTs;

	@Column(name = "LAST_UPDATE_TS")
	private LocalDateTime lastUpdateTs;

	@JsonIgnore
	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "SANGAM_MEMBER_ID")
	private SangamMember sangamMember;

	@JsonIgnore
	@OneToMany(mappedBy = "loan", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<LoanPayment> loanPaymentList;
}
