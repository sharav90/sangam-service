/**
 * 
 */
package com.magalir.sangam.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.magalir.sangam.util.Constants;

import lombok.Data;

/**
 * @author Saravanan M
 * @created date 21-Apr-2020 9:34:32 pm
 */
@Data
@Entity
@Table(name = Constants.SANGAM_MEMBERS)
public class SangamMember {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "MOBILE")
	private String mobile;

	@Column(name = "AADHAR_NO")
	private String aadharNo;

	@Column(name = "ADDRESS")
	private String address;

	@Column(name = "DATE_OF_BIRTH")
	private LocalDate dateOfBirth;

	@Column(name = "IS_ACTIVE")
	private Boolean isActive;

	@Column(name = "CREATED_TS")
	private LocalDateTime createdTs;

	@Column(name = "LAST_UPDATE_TS")
	private LocalDateTime lastUpdateTs;

	@ManyToOne(cascade = CascadeType.ALL)
	@JoinColumn(name = "SANGAM_ID")
	private Sangam sangam;

	@OneToMany(mappedBy = "sangamMember", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Loan> loanList;

	@OneToMany(mappedBy = "sangamMember", cascade = CascadeType.ALL, fetch = FetchType.LAZY)
	private List<Saving> savingList;
}
