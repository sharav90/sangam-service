/**
 * 
 */
package com.magalir.sangam.entity;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Table;

import com.magalir.sangam.util.Constants;

import lombok.Data;

/**
 * @author Saravanan M
 * @created date 21-Apr-2020 2:54:51 pm
 */
@Data
@Entity
@Table(name = Constants.ORGANIZER)
public class Organizer {
	@Id
	@GeneratedValue(strategy = GenerationType.AUTO)
	@Column(name = "id")
	private Long id;

	@Column(name = "NAME")
	private String name;

	@Column(name = "PASSCODE")
	private String passcode;

	@Column(name = "MOBILE")
	private String mobile;

	@Column(name = "AADHAR_NO")
	private String aadharNo;

	@Column(name = "DATE_OF_BIRTH")
	private LocalDate dateOfBirth;

	@Column(name = "ADDRESS")
	private String address;

	@Column(name = "IS_ACTIVE")
	private Boolean isActive;

	@Column(name = "CREATED_TS")
	private LocalDateTime createdTs;

	@Column(name = "LAST_UPDATE_TS")
	private LocalDateTime lastUpdateTs;

	@OneToMany(mappedBy = "organizer", cascade = CascadeType.ALL, fetch = FetchType.EAGER)
	private List<Sangam> sangamList;
}
