/**
 * 
 */
package com.magalir.sangam.svc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magalir.sangam.entity.SangamMember;
import com.magalir.sangam.entity.Saving;
import com.magalir.sangam.repo.SangamMemberRepo;
import com.magalir.sangam.repo.SavingRepo;
import com.magalir.sangam.svc.SavingService;
import com.magalir.sangam.util.DateUtil;

/**
 * @author Saravanan M
 * @created date 24-Apr-2020 6:27:14 pm
 */
@Service
public class SavingServiceImpl implements SavingService {

	private @Autowired SangamMemberRepo sangamMemRepo;
	private @Autowired SavingRepo savingRepo;

	@Override
	public Saving paySaving(final Long sangamMemId, final Saving saving) {
		final SangamMember sangamMem = sangamMemRepo.findById(sangamMemId).get();
		saving.setSangamMember(sangamMem);
		saving.setCreatedTs(DateUtil.DATE_TIME_NOW);
		return savingRepo.save(saving);
	}
}
