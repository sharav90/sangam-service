/**
 * 
 */
package com.magalir.sangam.svc.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magalir.sangam.dto.OrganizerDto;
import com.magalir.sangam.entity.Organizer;
import com.magalir.sangam.repo.OrganizerRepo;
import com.magalir.sangam.svc.OrganizerService;
import com.magalir.sangam.util.CryptoUtil;
import com.magalir.sangam.util.DateUtil;
import com.magalir.sangam.util.MapperUtil;

/**
 * @author Saravanan M
 * @created date 24-Apr-2020 6:20:32 pm
 */
@Service
public class OrganizerServiceImpl implements OrganizerService {

	private @Autowired OrganizerRepo organizerRepo;

	@Override
	public OrganizerDto save(final OrganizerDto organizerDto) {
		final Organizer organizer = MapperUtil.mapDtoToOrganizer(organizerDto);
		organizer.setPasscode(CryptoUtil.getEncodePasscode(organizer.getName(), organizer.getPasscode()));
		organizer.setIsActive(true);
		organizer.setCreatedTs(DateUtil.DATE_TIME_NOW);
		organizer.setLastUpdateTs(DateUtil.DATE_TIME_NOW);
		return MapperUtil.mapOrganizerToDto(organizerRepo.save(organizer));
	}

	@Override
	public List<OrganizerDto> findAll() {
		final List<Organizer> organizerList = organizerRepo.findAll();
		return MapperUtil.mapOrganizerListToDto(organizerList);
	}

	@Override
	public OrganizerDto findById(final Long id) {
		final Optional<Organizer> organizer = organizerRepo.findById(id);
		return organizer.isPresent() ? MapperUtil.mapOrganizerToDto(organizer.get()) : null;
	}

	@Override
	public OrganizerDto findByName(final String name) {
		final Optional<Organizer> organizer = organizerRepo.findByName(name);
		return organizer.isPresent() ? MapperUtil.mapOrganizerToDto(organizer.get()) : null;
	}
}
