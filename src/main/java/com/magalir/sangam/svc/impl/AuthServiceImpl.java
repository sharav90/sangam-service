/**
 * 
 */
package com.magalir.sangam.svc.impl;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import com.magalir.sangam.entity.Organizer;
import com.magalir.sangam.jwt.JwtUserDetails;
import com.magalir.sangam.repo.OrganizerRepo;
import com.magalir.sangam.svc.AuthService;

/**
 * @author Saravanan M
 * @created date 04-May-2020 8:10:07 pm
 */
@Service
public class AuthServiceImpl implements AuthService {

	private @Autowired OrganizerRepo repo;

	public Optional<Organizer> findByUserName(final String username) {
		return repo.findByName(username);
	}

	@Override
	public UserDetails loadUserByUsername(final String username) throws UsernameNotFoundException {
		final Optional<Organizer> user = this.findByUserName(username);
		if (!user.isPresent()) {
			throw new UsernameNotFoundException(String.format("USER_NOT_FOUND '%s'.", username));
		}
		return mapUserToJwtUserDetails(user.get());
	}

	private JwtUserDetails mapUserToJwtUserDetails(final Organizer user) {
		final JwtUserDetails jwtUser = new JwtUserDetails(user.getId(), user.getName(), user.getPasscode(), "admin");
		return jwtUser;
	}
}
