/**
 * 
 */
package com.magalir.sangam.svc.impl;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magalir.sangam.dto.LoanDto;
import com.magalir.sangam.dto.SangamMemberDto;
import com.magalir.sangam.entity.Loan;
import com.magalir.sangam.entity.Organizer;
import com.magalir.sangam.entity.Sangam;
import com.magalir.sangam.entity.SangamMember;
import com.magalir.sangam.repo.LoanRepo;
import com.magalir.sangam.repo.OrganizerRepo;
import com.magalir.sangam.repo.SangamMemberRepo;
import com.magalir.sangam.repo.SangamRepo;
import com.magalir.sangam.svc.SangamService;
import com.magalir.sangam.util.DateUtil;
import com.magalir.sangam.util.MapperUtil;

/**
 * @author Saravanan M
 * @created date 24-Apr-2020 6:24:52 pm
 */
@Service
public class SangamServiceImpl implements SangamService {

	private @Autowired OrganizerRepo organizerRepo;
	private @Autowired SangamRepo sangamRepo;
	private @Autowired SangamMemberRepo sangamMemRepo;
	private @Autowired LoanRepo loanRepo;

	@Override
	public Sangam createSangam(final Long organizerId, final Sangam sangam) {
		final Optional<Organizer> organizer = organizerRepo.findById(organizerId);
		sangam.setOrganizer(organizer.get());
		sangam.setIsActive(true);
		sangam.setCreatedTs(DateUtil.DATE_TIME_NOW);
		sangam.setLastUpdateTs(DateUtil.DATE_TIME_NOW);
		return sangamRepo.save(sangam);
	}

	@Override
	public SangamMember createSangamMember(final Long sangamId, final SangamMember sangamMember) {
		final Sangam sangam = sangamRepo.findById(sangamId).get();
		sangamMember.setSangam(sangam);
		sangamMember.setIsActive(true);
		sangamMember.setCreatedTs(DateUtil.DATE_TIME_NOW);
		sangamMember.setLastUpdateTs(DateUtil.DATE_TIME_NOW);
		return sangamMemRepo.save(sangamMember);
	}

	@Override
	public List<SangamMemberDto> findAllSangamMembers() {
		final List<SangamMember> sangamMemberList = sangamMemRepo.findAll();
		return MapperUtil.mapSangammeberListToDto(sangamMemberList);
	}

	@Override
	public SangamMemberDto findSangamMemberById(final Long sangamMemId) {
		final Optional<SangamMember> sangamMem = sangamMemRepo.findById(sangamMemId);
		return sangamMem.isPresent() ? MapperUtil.mapSangamMemberToDto(sangamMem.get()) : null;
	}

	@Override
	public LoanDto findLoanInfoByLoanId(final Long sangamMemId, final Long loanId) {
		final LoanDto loanDto;
		final Optional<Loan> loan = loanRepo.findById(loanId);
		if (loan.isPresent()) {
			loanDto = MapperUtil.mapLoanToDto(loan.get());
		} else {
			loanDto = null;
		}
		return loanDto;
	}
}
