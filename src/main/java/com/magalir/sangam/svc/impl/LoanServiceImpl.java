/**
 * 
 */
package com.magalir.sangam.svc.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.magalir.sangam.entity.Loan;
import com.magalir.sangam.entity.LoanPayment;
import com.magalir.sangam.entity.SangamMember;
import com.magalir.sangam.repo.LoanPaymentRepo;
import com.magalir.sangam.repo.LoanRepo;
import com.magalir.sangam.repo.SangamMemberRepo;
import com.magalir.sangam.svc.LoanService;
import com.magalir.sangam.util.DateUtil;

/**
 * @author Saravanan M
 * @created date 24-Apr-2020 6:28:48 pm
 */
@Service
public class LoanServiceImpl implements LoanService {

	private @Autowired SangamMemberRepo sangamMemRepo;
	private @Autowired LoanRepo loanRepo;
	private @Autowired LoanPaymentRepo loanPayRepo;

	@Override
	public Loan createLoan(final Long sangamMemId, final Loan loan) {
		final SangamMember sangamMem = sangamMemRepo.findById(sangamMemId).get();
		loan.setSangamMember(sangamMem);
		loan.setIsActive(true);
		loan.setCreatedTs(DateUtil.DATE_TIME_NOW);
		loan.setLastUpdateTs(DateUtil.DATE_TIME_NOW);
		return loanRepo.save(loan);
	}

	@Override
	public LoanPayment payLoan(final Long loanId, final LoanPayment loanPayment) {
		final Loan loan = loanRepo.findById(loanId).get();
		loanPayment.setLoan(loan);
		loanPayment.setCreatedTs(DateUtil.DATE_TIME_NOW);
		return loanPayRepo.save(loanPayment);
	}
}
