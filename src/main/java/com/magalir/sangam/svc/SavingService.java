/**
 * 
 */
package com.magalir.sangam.svc;

import com.magalir.sangam.entity.Saving;

/**
 * @author Saravanan M
 * @created date 24-Apr-2020 6:26:55 pm
 */
public interface SavingService {

	Saving paySaving(final Long sangamMemId, final Saving saving);
}
