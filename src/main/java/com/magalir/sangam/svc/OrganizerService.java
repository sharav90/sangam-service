/**
 * 
 */
package com.magalir.sangam.svc;

import java.util.List;

import com.magalir.sangam.dto.OrganizerDto;

/**
 * @author Saravanan M
 * @created date 24-Apr-2020 6:18:59 pm
 */
public interface OrganizerService {

	public OrganizerDto save(final OrganizerDto organizer);

	public List<OrganizerDto> findAll();

	public OrganizerDto findById(final Long id);

	public OrganizerDto findByName(final String name);
}
