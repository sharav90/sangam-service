/**
 * 
 */
package com.magalir.sangam.svc;

import com.magalir.sangam.entity.Loan;
import com.magalir.sangam.entity.LoanPayment;

/**
 * @author Saravanan M
 * @created date 24-Apr-2020 6:28:24 pm
 */
public interface LoanService {

	Loan createLoan(final Long sangamMemId, final Loan loan);

	LoanPayment payLoan(final Long loanId, final LoanPayment loanPayment);
}
