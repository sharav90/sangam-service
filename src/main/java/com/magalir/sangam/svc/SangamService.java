/**
 * 
 */
package com.magalir.sangam.svc;

import java.util.List;

import com.magalir.sangam.dto.LoanDto;
import com.magalir.sangam.dto.SangamMemberDto;
import com.magalir.sangam.entity.Sangam;
import com.magalir.sangam.entity.SangamMember;

/**
 * @author Saravanan M
 * @created date 24-Apr-2020 6:24:07 pm
 */
public interface SangamService {

	Sangam createSangam(final Long organizerId, final Sangam sangam);

	SangamMember createSangamMember(final Long sangamId, final SangamMember sangamMember);

	List<SangamMemberDto> findAllSangamMembers();

	SangamMemberDto findSangamMemberById(final Long sangamMemId);

	LoanDto findLoanInfoByLoanId(final Long sangamMemId, final Long loanId);

}
