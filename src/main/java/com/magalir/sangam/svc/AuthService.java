/**
 * 
 */
package com.magalir.sangam.svc;

import java.util.Optional;

import org.springframework.security.core.userdetails.UserDetailsService;

import com.magalir.sangam.entity.Organizer;

/**
 * @author Saravanan M
 * @created date 04-May-2020 8:07:25 pm
 */
public interface AuthService extends UserDetailsService {

	Optional<Organizer> findByUserName(final String username);
}
